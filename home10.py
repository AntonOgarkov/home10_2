import random
import matplotlib.pyplot as plt
import time
empty_student = {'name': '',
                  'second_name': '',
                  'group': '',
                  'age': 0,
                  'year_final': 0,
                  'salary': 0
                  }
print_order = ['name', 'second_name', 'age', 'group', 'year_final', 'salary']
SnHash = []
students = []



def add_student(student = None):
    if student == None:
        students.append(empty_student.copy())
        current_student = students[-1]
        for key in current_student:
            current_student[key] = input('Enter ' + key + ':')
            if type(empty_student[key]) == type(1):
                current_student[key] = int(current_student[key])
    else:
        a = 0
        h = hash(student['name']+student['second_name'])
        if len(SnHash) !=0:
            b = len(SnHash) - 1
            s = True
            while s:
                m = (a + b) // 2
                if a == m or b == m:
                    if h == SnHash[m][0]:
                        #print(SnHash[m][1]['name'])
                        if student['name'] == SnHash[m][1]['name']:
                            #print(SnHash)
                            #print(student)
                            s = False
                            return 0
                        else:
                            SnHash[m].append(student)
                            #print('collision')
                            s = False
                            return 1
                    else:
                        #print('ok')
                        students.append(student)
                        SnHash.insert(m, [h, student])
                        s = False
                        return 1
                elif h > SnHash[m][0]:
                    a = m
                else:
                    b = m
        else:
            #print('first')
            students.append(student)
            SnHash.append([h, student])
            return 1


def remove_student():
    print('Remove student')
    name = input('Enter name for deletion:')
    second_name = input('Enter second name for deletion:')
    age = input('Enter age for deletion:')
    found_student = None
    for student in students:
        if (name == student['name'] and
                second_name == student['second_name'] and
                age == str(student['age'])):
            found_student = student
            break
    if found_student != None:
        students.remove(found_student)
    pass


def dump_student_database():
    f = open('students.db', 'w')
    f.write(str(students))
    f.close()
    f = open('students_and_hash.db', 'w')
    f.write(str(SnHash))
    f.close()

# def load_student_database():
#     try:
#         f = open('students.db', 'r')
#         global students
#         students = eval(f.readline())
#         f.close()
#     except NameError:
#         pass

def print_students():
    print('Database has ' + str(len(students)) + ' students')
    for student in students:
        print('---------------')
        for key in print_order:
            pad = ' ' * (len('second_name') - len(key))
            print(key, pad, student[key])


# load_student_database()
while True:
    regime = input('Enter mode (1 - add, 2 - remove, 3 - printout, 4 - generate,5 - dump):')
    if regime == '1':
        add_student()
        dump_student_database()
    elif regime == '2':
        remove_student()
        dump_student_database()
    elif regime == '3':
        print_students()
    elif regime == '4':
        folder = "students.txt"
        f = open(folder)
        names = f.readlines()
        l = len(names)
        t = []
        t.append(time.clock())
        for j in range(1000):
            i=0
            while i < 1000:
                student = {'name': names[random.randint(0,l-1)][:-1],
                                 'second_name': names[random.randint(0,l-1)][:-1],
                                 'group': '',
                                 'age': random.randint(18,23),
                                 'year_final': 0,
                                 'salary': random.randint(10000,100000)
                                 }
                i += add_student(student)
                # print(student['name'],student['second_name'])
            t.append(time.clock()-t[j-1])
        print(t)
        plt.plot(t[1:])
        plt.show()
        print(len(SnHash))

    elif regime == '5':
        dump_student_database()